#!/bin/bash

GPIO_FILE=/sys/class/gpio/gpio26/value
if test -f "$GPIO_FILE"; then
    echo "$GPIO_FILE exists."
else 
	echo "$GPIO_FILE creating and setting as input"
	echo 26 > /sys/class/gpio/export
	echo in > /sys/class/gpio/gpio26/direction
fi

PRESS_CNTR=1
LEDS_ON=0
RED_LED=16
GREEN_LED=20
BLUE_LED=12

echo 16 > /sys/class/gpio/export
echo 20 > /sys/class/gpio/export
echo 12 > /sys/class/gpio/export

echo out > /sys/class/gpio/gpio16/direction
echo out > /sys/class/gpio/gpio20/direction
echo out > /sys/class/gpio/gpio12/direction


red_on()
{
 echo 1 > /sys/class/gpio/gpio16/value
}

green_on()
{
 echo 1 > /sys/class/gpio/gpio20/value
}

blue_on()
{
 echo 1 > /sys/class/gpio/gpio12/value
}

red_off()
{
 echo 0 > /sys/class/gpio/gpio16/value
}

green_off()
{
 echo 0 > /sys/class/gpio/gpio20/value
}

blue_off()
{
 echo 0 > /sys/class/gpio/gpio12/value
}
blink_delay()
{
	sleep 0.5
}

blink_red() 
{
	red_on
	blink_delay
	red_off
}

blink_green()
{
	green_on
	blink_delay
	green_off
}

blink_blue()
{
	blue_on
	blink_delay
	blue_off
}

blink_leds()
{
	if [ "$(( PRESS_CNTR % 2 ))" -eq 0 ]; then
		echo "Blinking forward"
		blink_red
		blink_green
		blink_blue
	fi
	if [ "$(( PRESS_CNTR % 2 ))" -eq 1 ]; then
		echo "Blinking backward"
		blink_blue
		blink_green
		blink_red
	fi
}

while  true ; do
	PREV_BUTTON_STATE=$( cat $GPIO_FILE )
	sleep 0.1
	GPIO_STATE_BUTTON=$( cat $GPIO_FILE )
	if [[ ( "$GPIO_STATE_BUTTON" == 1 ) && ( "$GPIO_STATE_BUTTON" != "$PREV_BUTTON_STATE" ) ]]; then
			if [[ $PRESS_CNTR == 1 ]]; then
				LEDS_ON=1
			fi
			echo "Button is pressed"
			echo PRESS_CNTR: $PRESS_CNTR
			((PRESS_CNTR=PRESS_CNTR+1))
			if [[ ( $LEDS_ON != 0 ) ]]; then
				echo "Blinking the LEDS"
				blink_leds
			fi
	fi
done