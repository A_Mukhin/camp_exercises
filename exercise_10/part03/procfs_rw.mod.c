#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0x3364d393, "module_layout" },
	{ 0x2b68b15f, "kobject_put" },
	{ 0xa2de42b1, "kmalloc_caches" },
	{ 0x2b289bac, "remove_proc_entry" },
	{ 0x837b7b09, "__dynamic_pr_debug" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0x81515fc2, "kobject_create_and_add" },
	{ 0x6b10bee1, "_copy_to_user" },
	{ 0xc3746423, "proc_mkdir" },
	{ 0xc5850110, "printk" },
	{ 0xbcab6ee6, "sscanf" },
	{ 0xbdfb6dbb, "__fentry__" },
	{ 0xeaa272b3, "kmem_cache_alloc_trace" },
	{ 0xa93ab294, "kernel_kobj" },
	{ 0x37a0cba, "kfree" },
	{ 0x226321ee, "proc_create" },
	{ 0x89991a0b, "sysfs_create_file_ns" },
	{ 0x13c49cc2, "_copy_from_user" },
	{ 0x88db9f48, "__check_object_size" },
};

MODULE_INFO(depends, "");


MODULE_INFO(srcversion, "A19A770EC65E22B271D1383");
