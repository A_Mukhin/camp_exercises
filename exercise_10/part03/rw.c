
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/sysfs.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Aleksandr Bulyshchenko <A.Bulyshchenko@globallogic.com>");
MODULE_DESCRIPTION("Example for procfs read/write and setting of the parameters from sysfs");
MODULE_VERSION("0.1");


#define MODULE_TAG      "example_module "
#define PROC_DIRECTORY  "example"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     10
#define REVER_STRING    1

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static struct kobject *example_kobject;


static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static const struct proc_ops proc_fops = {
    .proc_read  = example_read,
    .proc_write = example_write,
};

static int foo;

static ssize_t foo_show(struct kobject *kobj, struct kobj_attribute *attr,
                      char *buf)
{
        return sprintf(buf, "%d\n", foo);
}

static ssize_t foo_store(struct kobject *kobj, struct kobj_attribute *attr,
                      const char *buf, size_t count)
{
        sscanf(buf, "%du", &foo);
        return count;
}


static struct kobj_attribute foo_attribute =__ATTR(foo, 0660, foo_show,
                                                   foo_store);
static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}


static int create_proc_example(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}

static int create_sysfs_example(void)
{
    int error = 0;
    example_kobject = kobject_create_and_add("kobject_example",
                                            kernel_kobj);
    if(!example_kobject)
          return -ENOMEM;

    error = sysfs_create_file(example_kobject, &foo_attribute.attr);

    if (error) {
         pr_debug("failed to create the foo file in /sys/kernel/kobject_example \n");
   }
   return error;
}

static char *strrev(char *str)
{
      char *p1, *p2;

      if (! str || ! *str)
            return str;
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }
      return str;
}

static void cleanup_proc_example(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
    kobject_put(example_kobject);
}


static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    size_t left;
    
    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);
    if (foo == REVER_STRING) {
        pr_info("String reverted\n");
        proc_buffer = strrev(proc_buffer);
    }
    left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);


    proc_msg_read_pos += length - left;
    if (left)
        pr_err(MODULE_TAG "failed to read %lu from %lu chars\n", left, length);
    else
        pr_notice(MODULE_TAG "read %lu chars\n", length);

    return length - left;
}


static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;


    if (length > BUFFER_SIZE)
    {
        pr_warn(MODULE_TAG "reduse message length from %lu to %u chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;




    left = copy_from_user(proc_buffer, buffer, msg_length);


    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %lu from %lu chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %lu chars\n", msg_length);

    return length;
}


static int __init example_init(void)
{
    int err;

    err = create_buffer();
    if (err)
        goto error;

    err = create_proc_example();
    if (err)
        goto error;

    err = create_sysfs_example();
    if (err)
        goto error;
    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_example();
    cleanup_buffer();
    return err;
}


static void __exit example_exit(void)
{
    cleanup_proc_example();
    cleanup_buffer();
    printk(KERN_NOTICE MODULE_TAG "exited\n");
}


module_init(example_init);
module_exit(example_exit);
