
#!/bin/bash

GPIO_FILE=/sys/class/gpio/gpio26/value
if test -f "$GPIO_FILE"; then
    echo "$GPIO_FILE exists."
else 
        echo "$GPIO_FILE creating and setting as input"
        echo 26 > /sys/class/gpio/export
        echo in > /sys/class/gpio/gpio26/direction
fi

get_time()
{
    HOURS=$(i2cget -y 1 0x68 0x02)
    MIUNTES=$(i2cget -y 1 0x68 0x01)
    SEC=$(i2cget -y 1 0x68 0x00)
    echo "$(($HOURS)):$(($MIUNTES)):$(($SEC))"
}
BUTTON_PRESSED=0
check_button_state()
{
        PREV_BUTTON_STATE=$( cat $GPIO_FILE )
        sleep 0.1
        GPIO_STATE_BUTTON=$( cat $GPIO_FILE)
        if [[ ( "$GPIO_STATE_BUTTON" == 1 ) && ( "$GPIO_STATE_BUTTON" == "$PREV_BUTTON_STATE" ) ]];
        then
        BUTTON_PRESSED=1
        else
        BUTTON_PRESSED=0
        fi
}
ONE_SEC=1000
START_TIME=$( echo $(($(date +%s%N)/1000000)))
while  true ; do
        check_button_state
        CURRENT_TIME=$( echo $(($(date +%s%N)/1000000)))
        if [[ "$BUTTON_PRESSED" == 1  ]]; then
                BUTTON_PRESS_TIME=$(( CURRENT_TIME - START_TIME))
                echo "button is pressed now for $BUTTON_PRESS_TIME ms"
                if [[ "$BUTTON_PRESS_TIME" -lt "$ONE_SEC" ]]; then
                #get time from real time module
                get_time
                else
                #turn off
                echo "Button is pressed for $BUTTON_PRESS_TIME, whu is  more than one second, exit"
                exit
                fi
        else
        START_TIME=$( echo $(($(date +%s%N)/1000000)))
        fi
done
