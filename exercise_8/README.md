# Buildinng a kernel

1.  create shallow clone of the sources

	*git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable*

2. Prepare and build

 	*export BUILD_KERNEL=/home/strngr/linux-stable*

	*make ARCH=i386 O=${BUILD_KERNEL} defconfig*

	*cd ${BUILD_KERNEL}*

3. Configure kernel (if needed)

	*make menuconfig*

4. Building 

	*make -j$(nproc)*

# Buildroot

1. *git clone git://git.buildroot.net/buildroot*

2. *cd buildroot*

3. *export BUILD_ROOTFS=/home/strngr/buildroot*
	
4. *make O=${BUILD_ROOTFS} qemu_x86_defconfig*

5. *cd ${BUILD_ROOTFS}*

## Configure Buildroot 

1. *make menuconfig*

### Choose in GUI menu:

Target options:

You can get the guide how to set it on [link](https://gl-khpi.gitlab.io/assets/pdf/Kernel_building.pdf)

Note: You can find your kernel current kernel sources version my running *make kernelversion*

# Launch QUEMU

1. Intall QUEMU: *sudo apt install qemu-system-x86*

2. Command to run the kernel using qemu: 
* qemu-system-i386 -kernel ${BUILD_KERNEL}/arch/x86_64/boot/bzImage -append "root=/dev/sda console=ttyS0" -drive format=raw,file=/home/strngr/buildroot/output/images/rootfs.ext2 -nic user,hostfwd=tcp::8022-:22 &*