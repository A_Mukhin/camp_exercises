#!/bin/bash
echo "author: Anton Mukhin"
echo "Second exercise"
before=$(ls)
find -mtime 30 -print0 | xargs --null -I{} mv {} {}~
after=$(ls)
if [[ "$before" == "$after" ]]; then
	echo "no filenames was changed"
	bash ../exercise1/task1.sh
fi
