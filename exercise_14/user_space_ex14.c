#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <stdint.h>
#include <stdint.h>
#include <alloca.h>

uint64_t get_posix_clock_time()
{
    struct timespec ts;

    if (clock_gettime(CLOCK_MONOTONIC, &ts) == 0)
        return (uint64_t)(ts.tv_sec * 1000000000 + ts.tv_nsec);
    else
        return 0;
}

#define MAX_ALLOCATIONS_NUM    100
#define ALLOCATION_SIZE_INCREM 100

int main(void)
{
    uint64_t start;
    uint64_t end;
    uint64_t alloc_time;
    uint64_t free_time;
    char *   buff = NULL;
    size_t   i    = 0;

    printf("Malloc allocation\n");
    printf("Buff size\tAlloc time\tFree time\n");
    for (i = 1; i <= MAX_ALLOCATIONS_NUM; i++) {
        start      = get_posix_clock_time();
        buff       = malloc(sizeof(char) * ALLOCATION_SIZE_INCREM * i);
        end        = get_posix_clock_time();
        alloc_time = end - start;
        if (!buff) {
            printf("Failed to allocate the buffer\n");
            return -1;
        }

        start = get_posix_clock_time();
        free(buff);
        end       = get_posix_clock_time();
        free_time = end - start;
        printf("%ld\t\t%ld\t\t%ld\n",
               sizeof(char) * ALLOCATION_SIZE_INCREM * i,
               alloc_time,
               free_time);
    }

    printf("Calloc allocation\n");
    printf("Buff size\tAlloc time\tFree time\n");
    for (i = 1; i <= MAX_ALLOCATIONS_NUM; i++) {
        start      = get_posix_clock_time();
        buff       = calloc(ALLOCATION_SIZE_INCREM * i, sizeof(char));
        end        = get_posix_clock_time();
        alloc_time = end - start;
        if (!buff) {
            printf("Failed to allocate the buffer\n");
            return -1;
        }

        start = get_posix_clock_time();
        free(buff);
        end       = get_posix_clock_time();
        free_time = end - start;
        printf("%ld\t\t%ld\t\t%ld\n",
               sizeof(char) * ALLOCATION_SIZE_INCREM * i,
               alloc_time,
               free_time);
    }

    printf("Alloca allocation\n");
    printf("Buff size\tAlloc time\tFree time\n");
    for (i = 1; i <= MAX_ALLOCATIONS_NUM; i++) {
        start      = get_posix_clock_time();
        buff       = alloca(ALLOCATION_SIZE_INCREM * i * sizeof(char));
        end        = get_posix_clock_time();
        alloc_time = end - start;
        if (!buff) {
            printf("Failed to allocate the buffer\n");
            return -1;
        }

        printf("%ld\t\t%ld\t\t-\n",
               sizeof(char) * ALLOCATION_SIZE_INCREM * i,
               alloc_time);
    }
}