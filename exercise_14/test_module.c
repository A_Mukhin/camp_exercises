
#include <asm/uaccess.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anton Mukhin");
MODULE_DESCRIPTION("Exercise 14 task implementation");

#define ALLOCATION_SIZE_INCREM 100
#define MAX_ALLOCATIONS_NUM    100
#define PAGES_TO_ALLOC_POW     8
int __init init_ex14(void)
{
    ktime_t       start;
    ktime_t       end;
    ktime_t       alloc_time;
    ktime_t       free_time;
    size_t        i;
    char *        buff = NULL;
    unsigned long page = 0;

    pr_info("KMalloc allocation\n");
    pr_info("Buff size\tAlloc time\tFree time");
    for (i = 1; i <= MAX_ALLOCATIONS_NUM; i++) {
        start = ktime_get();
        buff  = kmalloc(sizeof(char) * ALLOCATION_SIZE_INCREM * i, GFP_KERNEL);
        end   = ktime_get();
        alloc_time = end - start;
        if (!buff) {
            pr_err("Failed to allocate the buffer\n");
            return -ENOMEM;
        }

        start = ktime_get();
        kfree(buff);
        end       = ktime_get();
        free_time = end - start;
        pr_info("%ld\t\t%lld\t\t%lld\n",
                sizeof(char) * ALLOCATION_SIZE_INCREM * i,
                alloc_time,
                free_time);
    }
    pr_info("Kzalloc allocation\n");
    pr_info("Buff size\tAlloc time\tFree time");
    for (i = 1; i <= MAX_ALLOCATIONS_NUM; i++) {
        start = ktime_get();
        buff  = kzalloc(sizeof(char) * ALLOCATION_SIZE_INCREM * i, GFP_KERNEL);
        end   = ktime_get();
        alloc_time = end - start;
        if (!buff) {
            pr_err("Failed to allocate the buffer\n");
            return -ENOMEM;
        }

        start = ktime_get();
        kfree(buff);
        end       = ktime_get();
        free_time = end - start;
        pr_info("%ld\t\t%lld\t\t%lld\n",
                sizeof(char) * ALLOCATION_SIZE_INCREM * i,
                alloc_time,
                free_time);
    }
    pr_info("VMalloc allocation\n");
    pr_info("Buff size\tAlloc time\tFree time");
    for (i = 1; i <= MAX_ALLOCATIONS_NUM; i++) {
        start      = ktime_get();
        buff       = vmalloc(sizeof(char) * ALLOCATION_SIZE_INCREM * i);
        end        = ktime_get();
        alloc_time = end - start;
        if (!buff) {
            pr_err("Failed to allocate the buffer\n");
            return -ENOMEM;
        }

        start = ktime_get();
        vfree(buff);
        end       = ktime_get();
        free_time = end - start;
        pr_info("%ld\t\t%lld\t\t%lld\n",
                sizeof(char) * ALLOCATION_SIZE_INCREM * i,
                alloc_time,
                free_time);
    }

    pr_info("__get_free_pages allocation\n");
    pr_info("Pages num\tAlloc time\tFree time");
    for (i = 1; i <= PAGES_TO_ALLOC_POW; i++) {
        start      = ktime_get();
        page       = __get_free_pages(GFP_KERNEL, i);
        end        = ktime_get();
        alloc_time = end - start;
        if (!page) {
            pr_err("Failed to allocate the pages\n");
            return -ENOMEM;
        }

        start = ktime_get();
        free_pages(page, i);
        end       = ktime_get();
        free_time = end - start;
        pr_info("2^%ld\t\t%lld\t\t%lld\n", i, alloc_time, free_time);
    }

    return 0;
}

void __exit cleanup_ex14(void)
{
    pr_info("Ex14 module exit\n");
}

module_init(init_ex14);
module_exit(cleanup_ex14)
