#!/bin/bash
echo "author: Anton Mukhin"
echo "First task"

# Transform long options to short ones
for arg in "$@"; do
  shift
  case "$arg" in
    "--help") set -- "$@" "-h" ;;
    "--test") set -- "$@" "-t" ;;
    "--yes")   set -- "$@" "-y" ;;
    "--recursive")   set -- "$@" "-r" ;;
    *)        set -- "$@" "$arg"
  esac
done
rec_flag=false
test_flag=false
# Here we can parse all options and set flags for next usage 
while getopts "hryt" arg; do
	case "$arg" in
		"h") echo "printed the help option"
		exit 0
		;;
		"r") 
		echo "recursive option used"
		rec_flag=true
		;;
		"y")
		echo "yes option used, no need to answer on system questions"
		;;
		"t")
		echo "Test option used - path to files will be printed, but files will not be deleted"
		test_flag=true
		;;
		"?")
		echo "wrong option used"
		exit 1
		;;
	esac
done


echo "Creating files for the first part"
touch 1.tmp 2.tmp 3.tmp
ls *.tmp
if [[ $test_flag == true ]]; then
	ls *.tmp
else
	echo "Now we can remove files"
	if [[ $rec_flag == true ]]; then
	rm -rf *.tmp
else
	rm -f *-tmp
fi
fi

echo "Creating files for the second part"
touch  _test.txt ~test.txt text222.txt
ls -a #show 
x
if [[ $test_flag == true ]]; then
	ls [-_~]*
	else
	echo "Now we can remove files" 
		if [[ $rec_flag == true ]]; then
	rm -rf [-_~]* #remove

else
	rm -f [-_~]* #remove
fi
fi

ls -a #show cleared dir
touch test.txt

